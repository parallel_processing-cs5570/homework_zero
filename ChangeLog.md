# Changelog

All notable changes to the project are to be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

> Note that very little versioning is utilized within this project at present.

## [Unreleased]

This section documents all anticipated changes in the upcoming release(s). There are no expected additions at present.


## [Release No]: YYYY-MM-DD

This section provides a template for the changlog. Please provide a brief description of the changes made and document what was added, changed, removed, and fixed.

### Added:
+ Include what was added here (functionality, features, etc.)

### Changed
+ If appropriate, add any changes here that may be noteworthy or of importance (ie. API modifications, implementation modifications, etc.)

### Removed
+ Include of things removed (of importance) here. This may include test cases, class-functions, etc.

### Fixed
+ Include all bug fixes that were embedded in the commit/merge here, if you can. Perhaps reference the commit hash for this (possibly excessive).