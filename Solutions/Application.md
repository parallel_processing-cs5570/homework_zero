# A Parallel Computing Application


## Background

Parallel computing, primarily consisting of a shared memory-type, is heavily utilized in the nuclear industry and research regarding particle transport. Applications for such codes, such as MCNP6, SCALE6, GEANT4, etc., include modeling nuclear reactors, accelerators, space events, and more. The industry and researchers also uses parallel codes for multi-physics coupling to more accurately estimate transient conditions, such as when coolant is lost in a reactor that results in heating of the nuclear matter, affecting cross sections and thus affecting the fundamental reactions causing the heating. There is significant demand for highly parallel applications in the nuclear industry.


## The Application of Interest

The application I will emphasize aligns with my research, being the estimation of particle emission and interaction cross sections resulting from high energy particle collisions. Such collisions are often modeling with event generators. The MCNP6 particle transport code interfaces to several different event generators, however all of the event generators it presently interfaces to were developed on now legacy systems using the Fortran66 and Fortran77 language standards and, thus far, any attempt at parallelism would require significant work. These applications are littered with global memory and thus cannot be used in parallel applications. My research involves coupling, modernizing, and parallelizing the default event generators in MCNP6 (CEM and LAQGSM). The "new" code, now labeled as GSM, has been coupled and mostly modernized at this point. Modernizing in this context has consisted of utilizing modern Fortran syntax and language standards and features, as well as migrating the code to an object-oriented framework. The current state of the code may be parallized in some regimes, however needs more work prior to be fully parallelized (i.e. removal of more global memory).


## Application Assessment

The code may be parallelized in several ways but no one has done so yet due to the large amount of techincal debt in the code. This is a big failure of the well-regarded MCNP6 code as simulations become significantly slower, in real time, when utilizing these event generators. The GSM event generator is thought to achieve its objective well, where the model has been validated against a variety of experimentally obtained data from a "black-box" sort of approach. Potential software clients of the GSM event generator are thought to almost strictly utilize MPI (shared memory parallelism) and _some_ OpenMP for a parallel implementation, thus the GSM event generator will likely utilize a similar parallel framework for consistency. Having said that, the design of GSM is intended to be applicable to all types of parallelism. Utilization of data-parallel type parallelism is also highly desired to allow simulations on GPUs, providing a _huge_ advancement in the realm of particle transport, particularly for these computationally demanding codes.


## Application HPC Utilization

The MCNP6, Scale, and GEANT4 codes _can_ all be run on supercomputers. For example, MCNP6 is run on the Trinity supercomputer at LANL (Los Alamos Nat'l Lab.), ranked number 7 on the [Top 500 list](https://www.top500.org/lists/2019/06/). Although I can't verify this, I would argue that Scale6 is run on the Summit supercomputer at ORNL (Oak Ridge Nat'l Lab.), ranked at the top of the [Top 500 list](https://www.top500.org/lists/2019/06/).

Some papers have been published by the MCNP6 development team that have demonstrated the performance of MCNP6 when utilized in a parallel setting. Such a paper as the [High Performance Computing & Monte Carlo](https://laws.lanl.gov/vhosts/mcnp.lanl.gov/pdf_files/la-ur-04-4532.pdf) paper, published in 2004, demonstrates its performance when utilized in a parallel setting. From these papers, it seems that the application has scaled well, scaling to both many processors as well as on several nodes on supercomputers. Despite this, the primary bottleneck for the MCNP6 code lies in the speed of memory access, being the primary bottleneck for almost all particle transport codes.

My application of interest, the GSM event generator, has no parallel abilities at present. MCNP6, likely to use the GSM event generator, could benefit greatly from a parallelized implementation of the GSM event generator as it is computationally demanding, thus requiring MCNP6 to idly wait for it to finish each simulation, where a single MCNP6 simulation could utilize hundreds of thousands of GSM simulations.