# Chase Juneau (Biography)

Some details about me are listed below. I've organized things according to what readers *might* be interested in, so they can skip all the superfluos text if they like. I would do the same, so no hard feelings will be had here!


## About Me

I grew up in the Helena, MT., area and attended elementary, middle-, and high school there. I graduated high school in 2017, and married my high school lover immediately proceeding that! My wife and I then moved to Bozeman, MT., so that I could get some general engineering credits taken care of before attending Idaho State University to obtain a degree in Nuclear Engineering while paying out of state tuition. Prior to moving to Pocatello, my wife and I purposefully became pregnant with our son, born in December, 2014. Later, we bought a home in Pocatello, ID. and had a daughter in January, 2018. The summers preceeding and proceeding my daughter's birth we lived in Los Alamos, NM., for my work and discovered what it was like living there. I'm currently pursuing my Ph.D. regarding work I've done for my M.S. and some of my undergraduate research. Unfortunately for my wife, this means that I've been in school for our entire marriage! My family and I are excited to see what lies in wait for us in the coming years!


## Education

I'm what I would call a "lame duck" - all my degrees are and will be from the same university, [Idaho State University](https://www.isu.edu/)! The degrees awared to me are listed concisely below for the reader's reference:
+ Minor in Applied Mathematics (May, 2017)
+ B.S. in Nuclear Engineering (May, 2017)
+ B.S. in Physics (May, 2017)
+ M.S. in Nuclear Science and Engineering (Auguest, 2019)
+ Ph.D. in Nuclear Science and Engineering (Anticipated May, 2021)


## Work Experience

My work experience is restricted mostly to the academic and research realms. I have listed my work experiences, along with a short description of the work performed (excluding the "why"), below:

| Position                        | Employer                       | Dates                   | Brief Description                                                           |
| :------------------------------ | :----------------------------- | :---------------------- | :-------------------------------------------------------------------------- |
| Graduate Research Assistant     | Idaho State University         | Aug.,  2017 - Present   | Migrating legacy scientific Fortran codes (CEM, LAQGSM) to an advanced and scalable implementation. Additional work has included stochastically estimating particle flux via Functional Expansion Tallies. |
| Graduate Research Assistant     | Los Alamos National Laboratory | May,   2017 - Present   | Reduction of legacy code and creation of an API for the default MCNP event generators |
| Graduate Teaching Assistant     | Idaho State University         | Jan.,  2018 - May, 2018; Aug., 2019 - Dec., 2019 | Handling grading and helping students regarding course material within the NE department at the ISU |
| Research Assistant              | Idaho State University         | May,   2016 - May, 2017 | Interfacing the LAQGSM event generators to the CEM event generator, allowing the deprecation of the former. |
| Research Assistant              | Idaho State University         | Sept., 2015 - May, 2016 | Generation of detector parts for muon experiments via SolidWorks |
| Customer Service Representative | Convergys (now Concentrix)     | July,  2014 - May, 2016 | Provided techincal support to DirecTV customers. Don't just watch TV, DirecTV! (I still got it!) |


## Research Interests

My research interest focus primarily in creating highly robust, portable, and scalable particle transport codes as well as utilizing various particle transport applications to simulate the world and particle interactions with it! My current research focuses on migrating legacy Fortran code to an object-oriented and scalable one. I intend to add parallelism to the high-energy event generator that my research deals with, primarily utilizing MPI, while designing the code to work well with all types of parallelism (shared memory, distributed memory, and data parallel).


## What's in it for me you ask?

As stated previously, I'm hoping to get an understanding of how to write parallel applications and how to actually parallelize them! My field of study utilizes MPI quite frequently, so it'd be nice to have an emphasis on working with MPI to create parallel applications, however MPI is not the only type of parallelism that I can utilize in my field! GPU-based parallelism (data parallel) is starting to arise in my field as well, providing even smaller computation times for applications that have used it!


## That's All Folks!

I think that's all of the "ice breaker" topics I can think of currently. If you'd like to know more about me or my current work/research, feel free to [email](mailto:junechas@isu.edu) me!