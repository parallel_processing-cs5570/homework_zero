# Parallel Processing: Homework 0

This repository represents the work done for HW-0 of the [Parallel Processing course](https://elearn.isu.edu/moodle/course/view.php?id=41518).

## Problem Statement

The problem statement is copied to the [Homework_0](Homework_0.pdf) file, however the original is located on the course [Moodle page](https://elearn.isu.edu/moodle/mod/assign/view.php?id=1231479).


## Solution

The [Solutions](./Solutions) directory contains all appropriate files regarding the solution to this homework set. The files of main concern include:
+ [Biography](./Solutions/Biography.md)
+ [Application](./Solutions/Application.md)


## Contribute

When contributing to this repository, please first discuss the change you wish to make via issue, email, or any other method with the owners of this repository before making a change. Contributions to this code implementation may be submitted via merge requests to this repository.

Prior to creating a merge request, please ensure the following:

+ Ensure all added features are properly documented within the code according to the [Doxygen](http://www.doxygen.nl/)-compliant commenting practices utilized. Please keep all programming stylistically compliant with the remainder of the project.
+ Please ensure any install or build dependencies are removed.
+ Update the [ChangeLog](ChangeLog.md) file with details of changes to the interface, this includes new environment variables, exposed functions, useful file locations and container parameters, for example.
+ Merge requests may be merged in once the modifications have been reviewed and approved by all appropriate reviewers. The merge request will be merged once all comments made by the reviewer(s), if any, have been resolved and corrected accordingly.


## Questions?

For any questions about this project or details, please contact [Chase Juneau](mailto:junechas@isu.edu).